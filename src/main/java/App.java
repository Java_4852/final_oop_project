import controller.AdminController;
import controller.Authorization;
import controller.UserController;
import model.Inventory;
import service.ProductService;
import service.ProductServiceInter;


public class App {

    public static void main(String[] args) {
        Inventory Inventory = new Inventory();
        UserController userController = new UserController(Inventory);
        ProductServiceInter productService = new ProductService(Inventory);
        AdminController adminController = new AdminController(productService);

        Authorization authorization = new Authorization( userController, adminController, productService);

        authorization.start();
    }
}
