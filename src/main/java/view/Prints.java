package view;

public class Prints {

    public static void displayAdminCommands() {
        printHeader();
        printMenuItems();
        printFooter();
    }
    public static void displayUserCommands() {
        printHeader();
        printUserItems();
        printFooter();
    }
    public static void displayAuthInfo() {
        System.out.println("<=================================>");
        System.out.println("      Warehouse Search System      ");
        System.out.println("<=================================>");
        System.out.println("Please enter your command('login','register','exit')");

    }
    public static void displayUsernameExistsMessage() {
        System.out.println("Username already exists. Please try again.");
    }
    public static void displayRegistrationSuccessMessage() {
        System.out.println("Registration successful. Please login to continue.");
    }
    public static void displaySearchingMenu() {
        displaySearchMenu();
        printFooter();
    }
    private static void printHeader() {
        System.out.println("<=================================>");
        System.out.println("         Warehouse Menu          ");
        System.out.println("<=================================>");
    }

    private static void printMenuItems() {
        System.out.println("Available Commands:");
        System.out.println(" - add:    Add a new product to the warehouse");
        System.out.println(" - view:   View details of a specific product");
        System.out.println(" - update: Update information of an existing product");
        System.out.println(" - delete: Delete a product from the warehouse");
        System.out.println(" - search: Search for products based on different parameters");
        System.out.println(" - list:   List all products in the warehouse");
        System.out.println(" - save:   save changes to the database ");
        System.out.println(" - commands:  Warehouse Menu");
        System.out.println(" - info:   Application Info");
        System.out.println(" - exit:   Exit the application");
    }

    private static void printUserItems() {
        System.out.println("Available User Commands:");
        System.out.println(" - view:   View details of a specific product");
        System.out.println(" - search: Search for products based on different parameters");
        System.out.println(" - list:   List all products in the warehouse");
        System.out.println(" - commands:  Warehouse User Menu");
        System.out.println(" - info:   Application Info");
        System.out.println(" - exit:   Exit the application");
    }

    public static void displayApplicationInfo() {
        System.out.println("Warehouse Search System v1.0 (Created on 2023-06-04)");
        System.out.println("Developer: Java");
        System.out.println("Email: criff913@gmail.com");
    }
    private static void printFooter() {
        System.out.println("<=================================>");
    }
    public static void displaySeparator() {
        System.out.println("<--------------------------------------------------------------------------------------------------------------------->");
    }

    private static void displaySearchMenu() {
        System.out.println("Available Commands for Search:");
        System.out.println(" - id:  Search by product id");
        System.out.println(" - name: Search by product name");
        System.out.println(" - category: Search by product category");
        System.out.println(" - quantity: Search by product quantity");
        System.out.println(" - price(range): Search by product price range");
        System.out.println(" - weight:  Search by product weight");
        System.out.println(" - dimensions: Search by product dimensions");
    }

    public static void exitMessage() {
        System.out.println("Exiting the application...");
    }

   public static void displayIncorrectPasswordMessage() {
        System.out.println("Incorrect password. Please try again.");
    }
    public static void invalidCommandMessage() {
        System.out.println("Invalid command. Please try again.");
    }

   public static void displayIncorrectLoginMessage() {
        System.out.println("Incorrect username or password. Please try again.");
    }

    public static void displayAdminName() {
        System.out.println("You cant use admin credentials");
    }
}
