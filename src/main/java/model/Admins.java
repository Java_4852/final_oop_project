package model;

public class Admins {
    private String login;
    private String password;
    public Admins(String login, String password) {
        this.login = login;
        this.password = password;
    }

    // Getters and Setters for login, password, and isAdmin
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    @Override
    public String toString() {
        return "Admin{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}