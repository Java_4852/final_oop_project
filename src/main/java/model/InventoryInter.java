package model;

import java.util.List;

public interface InventoryInter {
    void add(Product newProduct);

    void addProduct(Product product);

    void removeProduct(int productId);

    Product getProductById(int productId);

    List<Product> getAllProducts();

    void updateProduct(Product updatedProduct);

    List<Product> searchProductsByName(String name);

    List<Product> searchProductsByCategory(String category);

    List<Product> searchProductsByPriceRange(double minValue, double maxValue);

    List<Product> searchProductsByQuantity(int quantity);

    List<Product> searchProductsByManufacturer(String manufacturer);

    List<Product> searchProductsByWeight(double weight);

    List<Product> searchProductsByDimensions(String dimensions);

    void saveChanges();

    int getLastId();

    void addNewUser(User newUser);


    boolean UserExists(String username);

    boolean isUser(String username,String password);

    boolean isAdmin(String name ,String password);

}
