package model;


import model.DAL.FileWrite;
import model.DAL.LoadFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Inventory implements InventoryInter {
    private final Map<Integer, Product> inventory;
    private final Map<String, User> users;
    private final Map<String, Admins> admins;
    private int lastId;

    public Inventory() {
        this.users = new HashMap<>();
        this.admins = new HashMap<>();
        this.inventory = new HashMap<>();
        this.lastId = 0;
        LoadFile.loadInventoryData(this);
        LoadFile.loadUsersData(this.users);
        LoadFile.loadAdminsData(this.admins);
    }

    @Override
    public void add(Product newProduct) {
        inventory.put(newProduct.getId(), newProduct);
    }

    @Override
    public  void addProduct(Product product) {
        inventory.put(product.getId(), product);
    }

    @Override
    public void removeProduct(int productId) {
        inventory.remove(productId);
    }

    @Override
    public Product getProductById(int productId) {
        return inventory.get(productId);
    }

    @Override
    public List<Product> getAllProducts() {
        return new ArrayList<>(inventory.values());
    }

    @Override
    public void updateProduct(Product updatedProduct) {
        if (inventory.containsKey(updatedProduct.getId())) {
            inventory.put(updatedProduct.getId(), updatedProduct);
        }
    }

    @Override
    public List<Product> searchProductsByName(String name) {
        List<Product> searchResults = new ArrayList<>();
        for (Product product : inventory.values()) {
            if (product.getName().equalsIgnoreCase(name)) {
                searchResults.add(product);
            }
        }
        return searchResults;
    }

    @Override
    public List<Product> searchProductsByCategory(String category) {
        List<Product> searchResults = new ArrayList<>();
        for (Product product : inventory.values()) {
            if (product.getCategory().equalsIgnoreCase(category)) {
                searchResults.add(product);
            }
        }
        return searchResults;
    }

    @Override
    public List<Product> searchProductsByPriceRange(double minValue, double maxValue) {
        List<Product> searchResults = new ArrayList<>();
        for (Product product : inventory.values()) {
            if (product.getPrice() >= minValue && product.getPrice() <= maxValue) {
                searchResults.add(product);
            }
        }
        return searchResults;
    }

    @Override
    public List<Product> searchProductsByQuantity(int quantity) {
        List<Product> searchResults = new ArrayList<>();
        for (Product product : inventory.values()) {
            if (product.getQuantity() == quantity) {
                searchResults.add(product);
            }
        }
        return searchResults;
    }

    @Override
    public List<Product> searchProductsByManufacturer(String manufacturer) {
        List<Product> searchResults = new ArrayList<>();
        for (Product product : inventory.values()) {
            if (product.getManufacturer().equalsIgnoreCase(manufacturer)) {
                searchResults.add(product);
            }
        }
        return searchResults;
    }

    @Override
    public List<Product> searchProductsByWeight(double weight) {
        List<Product> searchResults = new ArrayList<>();
        for (Product product : inventory.values()) {
            if (product.getWeight() == weight) {
                searchResults.add(product);
            }
        }
        return searchResults;
    }

    @Override
    public List<Product> searchProductsByDimensions(String dimensions) {
        List<Product> searchResults = new ArrayList<>();
        for (Product product : inventory.values()) {
            if (product.getDimensions().equalsIgnoreCase(dimensions)) {
                searchResults.add(product);
            }
        }
        return searchResults;
    }

    @Override
    public void saveChanges() {
        FileWrite.writeInventoryToFile(inventory);
    }

    public int getLastId() {
        if (lastId == 0 && !inventory.isEmpty()) {
            for (Product product : inventory.values()) {
                if (product.getId() > lastId) {
                    lastId = product.getId();
                }
            }
        }
        return lastId;
    }

    @Override
    public void addNewUser(User newUser) {
        users.put(newUser.getLogin(), newUser);
        saveUserChanges();
    }
    public void saveUserChanges() {
        FileWrite.writeUserToFile(users);
    }
    @Override
    public boolean UserExists(String username) {
        return users.containsKey(username);

    }

    @Override
    public boolean isUser(String username, String password) {
        if (users.containsKey(username)) {
            User user = users.get(username);
            return user.getPassword().equals(password);
        }
        return false;
    }

    @Override
    public boolean isAdmin(String name, String password) {
        if (admins.containsKey(name)) {
            Admins admin = admins.get(name);
            return admin.getPassword().equals(password);
        }
        return false;
    }

}
