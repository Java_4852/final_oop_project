package model.DAL;

import model.Admins;
import model.Inventory;
import model.Product;
import model.User;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

public class LoadFile {
    public static void loadInventoryData(Inventory inventory) {
        String inventoryFilePath = "src/main/resources/inventory_data.csv";
        boolean skipFirstLine = true; // Flag to skip the first line (header)
        try (BufferedReader reader = new BufferedReader(new FileReader(inventoryFilePath))) {

            String line;
            int lineNumber = 0;
            while ((line = reader.readLine()) != null) {
                lineNumber++;

                if (skipFirstLine) {
                    skipFirstLine = false;
                    continue; // Skip the first line
                }

                String[] fields = line.split(";");
                if (fields.length != 8) {
                    System.out.println("Skipping line " + lineNumber + ": Empty lines in the Inventory: " + line);
                    continue;
                }

                try {
                    int id = Integer.parseInt(fields[0]);
                    String name = fields[1];
                    String category = fields[2];
                    double price = Double.parseDouble(fields[3]);
                    int quantity = Integer.parseInt(fields[4]);
                    String manufacturer = fields[5];
                    double weight = Double.parseDouble(fields[6]);
                    String dimensions = fields[7];

                    Product product = new Product(id, name, category, price, quantity, manufacturer, weight, dimensions);
                    inventory.addProduct(product);
                } catch (NumberFormatException e) {
                    System.out.println("Skipping line " + lineNumber + ": Invalid numeric value in the inventory CSV file: " + line);
                }
            }
            System.out.println("Inventory data loaded successfully from CSV file.");
        } catch (IOException e) {
            System.out.println("Error occurred while loading inventory data from CSV: " + e.getMessage());
        }
    }

    public static void loadUsersData(Map<String, User> users) {
        String usersFilePath = "src/main/resources/Users.csv";
        try (BufferedReader reader = new BufferedReader(new FileReader(usersFilePath))) {
            reader.readLine(); // Skip header line

            String line;
            int lineNumber = 1;
            while ((line = reader.readLine()) != null) {
                lineNumber++;

                String[] fields = line.split(";");
                if (fields.length != 2) {
                    System.out.println("Skipping line " + lineNumber + ": Invalid data format in the Users CSV file: " + line);
                    continue;
                }

                String login = fields[0];
                String password = fields[1];

                User user = new User(login, password);
                users.put(login, user);
            }

            System.out.println("User data loaded successfully from Users.csv file.");
        } catch (IOException e) {
            System.out.println("Error occurred while loading user data from Users.csv: " + e.getMessage());
        }
    }

    public static void loadAdminsData(Map<String, Admins> admins) {
        String adminsFilePath = "src/main/resources/AdminsList.csv";
        try (BufferedReader reader = new BufferedReader(new FileReader(adminsFilePath))) {
            reader.readLine(); // Skip header line

            String line;
            int lineNumber = 1;
            while ((line = reader.readLine()) != null) {
                lineNumber++;

                String[] fields = line.split(";");
                if (fields.length != 2) {
                    System.out.println("Skipping line " + lineNumber + ": Invalid data format in the Admins CSV file: " + line);
                    continue;
                }

                String login = fields[0];
                String password = fields[1];

                Admins admin = new Admins(login, password);
                admins.put(login, admin);
            }

            System.out.println("Admin data loaded successfully from Admins.csv file.");
        } catch (IOException e) {
            System.out.println("Error occurred while loading admin data from Admins.csv: " + e.getMessage());
        }
    }
}
