package model.DAL;

import model.Product;
import model.User;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class FileWrite {

    public static void writeInventoryToFile(Map<Integer, Product> inventory) {
        String filePath = "src/main/resources/inventory_data.csv"; // Change file extension to .csv
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            // Write the header line
            writer.write("ID;Name;Category;Price;Quantity;Manufacturer;Weight;Dimensions");
            writer.newLine();

            // Write each product as a line in the CSV file
            for (Product product : inventory.values()) {
                writer.write(product.getId() + ";" + product.getName() + ";" + product.getCategory() + ";" +
                        product.getPrice() + ";" + product.getQuantity() + ";" + product.getManufacturer() + ";" +
                        product.getWeight() + ";" + product.getDimensions());
                writer.newLine();
            }

            System.out.println("Inventory data saved successfully to CSV file.");
        } catch (IOException e) {
            System.out.println("Error occurred while saving inventory data to CSV: " + e.getMessage());
        }
    }
    public static void writeUserToFile(Map<String, User> users) {
        String usersFilePath = "src/main/resources/Users.csv";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(usersFilePath))) {
            // Write the header line
            writer.write("LOGIN;PASSWORD");
            writer.newLine();

            // Write each user as a line in the CSV file
            for (User user : users.values()) {
                writer.write(user.getLogin() + ";" + user.getPassword() );
                writer.newLine();
            }

            System.out.println("User data saved successfully to Users.csv file.");
        } catch (IOException e) {
            System.out.println("Error occurred while saving user data to Users.csv: " + e.getMessage());
        }
    }
}
