package utility;

public class Validator {
    public static boolean validateLoginType(String command) {
        if (command.isEmpty()) {
            return false;
        }
        String[] validCommands = { "Login", "Register","exit" };
        for (String validCommand : validCommands) {
            if (validCommand.equalsIgnoreCase(command)) {
                return true;
            }
        }
        return false;
    }

    public static boolean validateUsername(String username) {
        if (username.isEmpty()) {
            System.out.println("Invalid username. Please enter a username.");
            return false;
        }
        return true;
    }
    public static boolean validatePassword(String password) {
        if (password.isEmpty()) {
            System.out.println("Invalid password. Please enter a password.");
            return false;
        }
        return true;
    }
    public static boolean validateName(String name) {
        if (name.isEmpty()) {
            System.out.println("Invalid name. Please enter a valid name.");
            return false;
        }
        return true;
    }

    public static boolean validateCategory(String category) {
        if (category.isEmpty()) {
            System.out.println("Invalid category. Please enter a valid category.");
            return false;
        }
        return true;
    }

    public static boolean validatePrice(double price) {
        if (price <= 0) {
            System.out.println("Invalid price. Please enter a positive value for the price.");
            return false;
        }
        return true;
    }

    public static boolean validateQuantity(int quantity) {
        if (quantity <= 0) {
            System.out.println("Invalid quantity. Please enter a positive value for the quantity.");
            return false;
        }
        return true;
    }

    public static boolean validateManufacturer(String manufacturer) {
        if (manufacturer.isEmpty()) {
            System.out.println("Invalid manufacturer. Please enter a valid manufacturer.");
            return false;
        }
        return true;
    }

    public static boolean validateWeight(double weight) {
        if (weight <= 0) {
            System.out.println("Invalid weight. Please enter a positive value for the weight.");
            return false;
        }
        return true;
    }

    public static boolean validateDimensions(String dimensions) {
        if (dimensions.isEmpty()) {
            System.out.println("Invalid dimensions. Please enter valid dimensions.");
            return false;
        }
        return true;
    }

    public static boolean validateCommand(String command) {
        if (command.isEmpty()) {
            System.out.println("Invalid command. Please enter a command.");
            return false;
        }
        String[] validCommands = { "add", "view", "update", "delete", "search", "list", "exit", "save","info", "commands" };
        for (String validCommand : validCommands) {
            if (validCommand.equalsIgnoreCase(command)) {
                return true;
            }
        }
        System.out.println("Invalid command. Please enter a valid command.");
        return false;
    }



    public static boolean validateProductId(int productId) {
        if (productId <= 0) {
            System.out.println("Invalid ID. Please enter a value for the ID.");
            return false;
        }
        return true;
    }

    public static Boolean validateSearchParameter(String s) {
        String[] validParameters = {
                "id", "name", "category", "price", "quantity", "manufacturer", "weight", "dimensions"
        };

        for (String validParam : validParameters) {
            if (validParam.equalsIgnoreCase(s)) {
                return true;
            }
        }
        return false;
    }

    public static boolean validatePriceParameter(String s) {
        if (s == null || s.isEmpty()) {
            System.out.println("Invalid price range: Empty input.");
            return false;
        }

        String[] rangeValues = s.split("-");
        if (rangeValues.length > 2) {
            System.out.println("Invalid price range: More than one '-' symbol.");
            return false;
        }

        try {
            double minValue = Double.parseDouble(rangeValues[0].trim());
            if (minValue < 0) {
                System.out.println("Invalid price range: Minimum value cannot be negative.");
                return false;
            }

            if (rangeValues.length == 2) {
                double maxValue = Double.parseDouble(rangeValues[1].trim());
                if (maxValue < 0) {
                    System.out.println("Invalid price range: Maximum value cannot be negative.");
                    return false;
                }

                if (maxValue < minValue) {
                    System.out.println("Invalid price range: Maximum value cannot be less than the minimum value.");
                    return false;
                }
            }
        } catch (NumberFormatException e) {
            System.out.println("Invalid price range: Invalid numeric values.");
            return false;
        }

        return true;
    }


}
