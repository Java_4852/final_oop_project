package utility;

import model.Product;
import service.ProductService;
import view.InputHandler;

import java.util.List;

public class ObjectCreator {
    private final InputHandler inputHandler;
    private final ProductService productService;

    public ObjectCreator(InputHandler inputHandler, ProductService productService) {
        this.inputHandler = inputHandler;
        this.productService = productService;
    }

    private Product readProductDetails() {
        String name = inputHandler.getStringInput("Name", Validator::validateName);
        String category = inputHandler.getStringInput("Category", Validator::validateCategory);
        double price = inputHandler.getDoubleInput("Price", Validator::validatePrice);
        int quantity = inputHandler.getIntInput("Quantity", Validator::validateQuantity);
        String manufacturer = inputHandler.getStringInput("Manufacturer", Validator::validateManufacturer);
        double weight = inputHandler.getDoubleInput("Weight", Validator::validateWeight);
        String dimensions = inputHandler.getStringInput("Dimensions", Validator::validateDimensions);

        return new Product(0, name, category, price, quantity, manufacturer, weight, dimensions);
    }

    public Product createProductFromUserInput() {
        Product product = readProductDetails();

        int lastId = productService.getLastId();
        if (lastId == 0) {
            List<Product> products = productService.listAllProducts();
            if (!products.isEmpty()) {
                lastId = products.get(products.size() - 1).getId();
            }
        }
        int newProductId = lastId + 1;
        product.setId(newProductId);

        return product;
    }

    public Product updateProductFromUserInput(int productId) {
        Product product = readProductDetails();
        product.setId(productId);

        return product;
    }
}
