package controller;


import model.User;
import service.ProductServiceInter;
import view.Prints;
import view.InputHandler;
import utility.Validator;


public class Authorization {
    private final InputHandler inputHandler;
    private final ProductServiceInter productService;
    private final UserController userController;
    private final AdminController adminController;



    public Authorization(UserController userController, AdminController adminController,ProductServiceInter productService) {
        this.inputHandler = new InputHandler();
        this.userController = userController;
        this.adminController = adminController;
        this.productService = productService;
    }

    public void start() {
        Prints.displaySeparator();
        Prints.displayAuthInfo();

        while (true) {
            String command = inputHandler.getStringInput("Enter Command ",Validator::validateLoginType);

            switch (command.toLowerCase()) {
                case "register" -> registerUser();
                case "login" -> loginUser();
                case "exit" -> {
                    Prints.exitMessage();
                    return;
                }
                default -> Prints.invalidCommandMessage();
            }
        }
    }

    private void registerUser() {
        String username = inputHandler.getStringInput("Enter Username", Validator::validateUsername);
        String password = inputHandler.getStringInput("Enter Password", Validator::validatePassword);

        // Check if the username already exists
        if (productService.UserExists(username)) {
            Prints.displayUsernameExistsMessage();
            return;
        }
        if (productService.isAdmin(username, password)) {
            Prints.displayAdminName();
            return;
        }

        // Create a new user and add it to the list
        User newUser = new User(username, password);
        productService.addNewUser(newUser);
        Prints.displayRegistrationSuccessMessage();
        Prints.displaySeparator();
        Prints.displayAuthInfo();
    }

    private void loginUser() {
        String username = inputHandler.getStringInput("Enter Username", Validator::validateUsername);
        String password = inputHandler.getStringInput("Enter Password", Validator::validatePassword);

        // Find the user by username and check password

        boolean isAdmin = productService.isAdmin(username, password);
        boolean isUser;

        if (isAdmin) {
            System.out.println("Logged in as admin.");
            adminController.start();
        } else {
            isUser = productService.isUser(username, password);
            if (isUser) {
                System.out.println("Logged in as user.");
                userController.start();
            } else {
                System.out.println("Invalid credentials. Please try again.");
            }
        }
    }

}
