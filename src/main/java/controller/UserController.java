package controller;

import model.Inventory;
import model.Product;
import service.ProductService;
import view.InputHandler;
import utility.Validator;
import view.Prints;
import view.ProductView;

public class UserController {

    private final InputHandler inputHandler;
    private final SearchHandler searchHandler;
    private final ProductService productService;


    public UserController(Inventory inventoryRepository) {

        ProductService productService = new ProductService(inventoryRepository);
        this.inputHandler = new InputHandler();
        this.searchHandler = new SearchHandler(productService);
        this.productService = productService;
    }

    public void start() {

        Prints.displayUserCommands();
        while (true) {

            String command = inputHandler.getStringInput("Enter command", Validator::validateCommand);

            switch (command.toLowerCase()) {
                case "view" -> viewProductDetails();
                case "search" -> searchHandler.searchEngine();
                case "list" -> listAllProducts();
                case "commands" -> showCommands();
                case "info"-> Prints.displayApplicationInfo();
                case "exit" -> {
                    Prints.exitMessage();
                    Prints.displaySeparator();
                    Prints.displayAuthInfo();
                    return;
                }
                default -> Prints.invalidCommandMessage();
            }
            Prints.displaySeparator();
        }
    }


    private void showCommands() {
        Prints.displayUserCommands();
    }

    private void listAllProducts() {
        productService.listAllProducts();
    }


    public void viewProductDetails() {
        int productId = inputHandler.getIntInput("Enter product ID", Validator::validateProductId);
        Product product = productService.getProductById(productId);
        ProductView.displayProductDetails(product);
    }

}