package controller;

import model.Product;

import service.ProductService;
import view.Prints;
import view.InputHandler;
import utility.Validator;
import view.ProductView;
import service.ProductServiceInter;
import utility.ObjectCreator;

public class AdminController {

    private final InputHandler inputHandler;
    private final ProductServiceInter productService;
    private final ObjectCreator objectCreator;
    private final SearchHandler searchHandler;
    public AdminController(ProductServiceInter productService) {
        this.searchHandler = new SearchHandler((ProductService) productService);
        this.objectCreator = new ObjectCreator( new InputHandler(), (ProductService) productService);
        this.inputHandler = new InputHandler();
        this.productService = productService;

    }

    public void start() {

        Prints.displayAdminCommands();
        while (true) {

            String command = inputHandler.getStringInput("Enter command", Validator::validateCommand);

            switch (command.toLowerCase()) {
                case "add" -> addProduct();
                case "view" -> viewProductDetails();
                case "update" -> updateProduct();
                case "delete" -> deleteProduct();
                case "search" -> searchHandler.searchEngine();
                case "list" -> listAllProducts();
                case "save" -> saveChanges();
                case "commands" -> showCommands();
                case "info"-> Prints.displayApplicationInfo();
                case "exit" -> {
                    Prints.exitMessage();
                    Prints.displaySeparator();
                    Prints.displayAuthInfo();
                    return;
                }
                default -> Prints.invalidCommandMessage();
            }
            Prints.displaySeparator();
        }
    }

    private void showCommands() {
        Prints.displayAdminCommands();
    }

    private void listAllProducts() {
        productService.listAllProducts();
    }

    public void addProduct() {
        try {
            System.out.println("Enter product details:");
            Product newProduct = objectCreator.createProductFromUserInput();
            productService.add(newProduct);
            ProductView.displayProductDetails(newProduct);
            System.out.println("Product added successfully. Product ID: " + newProduct.getId());
        } catch (Exception e) {
            handleException("An error occurred while adding the product: " + e.getMessage());
        }
    }

    public void updateProduct() {
        try {
            int productId = inputHandler.getIntInput("Enter product ID", Validator::validateProductId);
            Product productToUpdate = productService.getProductById(productId);
            if (productToUpdate == null) {
                System.out.println("Product not found with the provided ID.");
                return;
            }
            ProductView.displayProductDetails(productToUpdate);
            System.out.println("Enter updated product details:");
            Product updatedProduct = objectCreator.updateProductFromUserInput(productId);
            productService.updateProduct(updatedProduct);
            System.out.println("Product updated successfully. Product ID: " + productId);
        } catch (Exception e) {
            handleException("An error occurred while updating the product: " + e.getMessage());
        }
    }

    public void deleteProduct() {
        int id = inputHandler.getIntInput("Enter product ID", Validator::validateProductId);
        Product product = productService.getProductById(id);

        if (product == null) {
            System.out.println("Product with ID " + id + " does not exist.");
        } else {
            ProductView.displayProductDetails(product);
            productService.deleteProduct(id);
            System.out.println("Product deleted successfully. Product ID: " + id);
        }
    }

    public void saveChanges() {
        try {
            productService.saveChanges();
            System.out.println("Changes saved successfully.");
        } catch (Exception e) {
            handleException("An error occurred while saving changes: " + e.getMessage());
        }
    }

    private void handleException(String errorMessage) {
        System.out.println(errorMessage);
    }

    public void viewProductDetails() {
        int productId = inputHandler.getIntInput("Enter product ID", Validator::validateProductId);
        Product product = productService.getProductById(productId);
        ProductView.displayProductDetails(product);
    }
}
